<?php
spl_autoload_register("autoload");

 function autoload($class):void
{
    $path = str_replace("\\", "/", $class);
    $validPath = $path . ".php";
    require $validPath;
    // echo $validPath . "<br/>";
}