<?php

namespace App\Http\Controller;

use App\Model\Product;
use Crawler\Http\Request;
use Regex\Pattern\Jumia;

class ProductController extends Product
{

    public function allProducts()
    {
        $product = new Product();
        $product->createTable();
        $data = $product->getData();
        //         var_dump($data);
        //         exit;
        return $data;
    }
    public function crawlProduct()
    {

        $req = new Request();
        $jumia = new Jumia();
        $res = $req->makeRequest("https://www.jumia.co.ke/mobile-phones/", $jumia);
        $save = new Product();
        $counter = 1;
        foreach ($res as $data) {
            //            $image = $req->downloadImage($data[1], "Storage/images", $counter);
            $product_data = [
                "name" => $data[0],
                "image" => $data[1],
                "initialPrice" => $data[2],
                "currentPrice" => $data[3]

            ];
            $save->insertData($product_data);
            $counter++;
        }
        if ($save) {
            echo "<script>alert(\"$save->crawlerMessage\")</script>";
        } else {
            echo "<script>alert(\"$save->crawlerMessage\")</script>";
        }
    }
}
