<?php

namespace App\Eloquent;

use App\Database\Db;

abstract class  Model extends Db
{
    public $status;
    public $crawlerMessage;
    public $apiMessage;

    // protected $tableFields = array();
    public function getData(string $table = null, int $id = null)
    {

        if (!is_null($table)) {
            $this->dbtable = $table;
        }
        if ($id == null) {
            $sql = "select * from $this->dbtable";
        } else {
            $sql = "select * from $this->dbtable where id=$id";
        }
        $data = array();
        $result = $this->conn->query($sql) or die("an error occurred!");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                $row->name=html_entity_decode($row->name);
                array_push($data, $row);
            }
//             print_r($data);
//             exit();
            $this->status = "success";
            return ($data);
        }
        $this->status = "error";
        $this->crawlerMessage = "NO user record was found in the $this->dbtable";
    }
    public function insertData(array $data)
    {
        $result = "";
        $name = $data["name"];
        $image = $data["image"];
        $initialPrice = $data["initialPrice"];
        $currentPrice = $data["currentPrice"];
        $sql = "select * from $this->dbtable where image='$image'";
        $res = $this->conn->query($sql) or die("an error occurred!");
        if ($res->num_rows > 0) {
            $sql = "UPDATE $this->dbtable SET name='$name',initialPrice='$initialPrice',currentPrice='$currentPrice' WHERE image='$image'";
            $result = $this->conn->query($sql) or die($this->conn->error);
            $this->status = "failed";
            $this->apiMessage = "Product already exists.";
            $this->crawlerMessage = "Your database is already in sync with the source";
        } else {
            $fields = implode(",", array_keys($data));
            $values = implode("','", array_values($data));
            $sql = "INSERT INTO  $this->dbtable ($fields) VALUES ('$values')";
            $result = $this->conn->query($sql) or die($this->conn->error);
            $this->status = "success";
            $this->apiMessage = "New product added successfully";
            $this->crawlerMessage = "Your database database has been updated successifully.";
        }
        return $result;
    }
    public function updateData(array $data, int $id = null)
    {
        if (!is_null($id)) {
            $productId = $id;
        }
        $result = null;
        $productId = $data['id'];
        $name = $data["name"];
        $image = $data["image"];
        $initialPrice = $data["initialPrice"];
        $currentPrice = $data["currentPrice"];
        $sql = "select * from $this->dbtable where id=$productId";
        $res = $this->conn->query($sql) or die("an error occurred!");
        if ($res->num_rows > 0) {
            $sql = "UPDATE $this->dbtable SET name='$name',image='$image',initialPrice='$initialPrice',currentPrice='$currentPrice' WHERE id=$productId";
            $result = $this->conn->query($sql) or die($this->conn->error);
            if ($result) {
                $this->status = "success";
                $this->apiMessage = "Product data updated successfully";
            } else {
                $this->status = "fail";
                $this->apiMessage = "Product data failed to update";
            }
            // return $result;
        } else {
            $this->status = "error";
            $this->apiMessage = "Product does not exist";
        }
        return $result;
    }
    public function deleteData(int $id)
    {
        $result = null;
        $sql = "select * from $this->dbtable where id=$id";
        $res = $this->conn->query($sql) or die($this->conn->error);
        if ($res->num_rows > 0) {
            $sql = "DELETE FROM $this->dbtable WHERE id=$id";
            $result = $this->conn->query($sql) or die($this->conn->error);
            if ($result) {
                $this->status = "success";
                $this->apiMessage = "Product deleted successfully";
                $this->crawlerMessage = "Product deleted successfully";
            } else {
                $this->status = "failed";
                $this->apiMessage = "Could not delete the record. Check your product id";
                $this->crawlerMessage = "Could not delete the record. Check your product id";
            }
            // return $result;
        } else {
            $this->status = "error";
            $this->apiMessage = "Product does not exist";
        }
        // var_dump($result);
        return $result;
    }
}