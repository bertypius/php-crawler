<?php

namespace App\Database;

set_include_path($_SERVER['DOCUMENT_ROOT'] . "/php-crawler/");
// require "Config/Db_Config.php";
require ".env";
abstract class Db
{
    private $host = HOST;
    private $user = USER;
    private $password = PASSWORD;
    private $dbname = DATABASE;
    protected $dbtable = "";
    protected $conn = "";

    /**
     * @return string
     */
    public function __construct()
    {
        $this->conn = new \mysqli($this->host, $this->user, $this->password);
        if (!$this->conn) {
            die($this->conn->connect_error);
        }
        $sql = "CREATE DATABASE IF NOT EXISTS `$this->dbname` ";
        $this->conn->query($sql) or die($this->conn->error);
        mysqli_select_db($this->conn, $this->dbname);
        $this->createTable();
    }
    abstract protected function getData(string $table = null, int $id = null);
    abstract protected function insertData(array $data);
    public function setTable($table)
    {
        $this->dbtable = $table;
    }
    public function createTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS $this->dbtable (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(200) NOT NULL,
            `image` varchar(100) NOT NULL,
            `initialPrice` varchar(100) NOT NULL,
            `currentPrice` varchar(100) NOT NULL,
            PRIMARY KEY (`id`)
         )";
        return $this->conn->query($sql);
    }
    public function getDatabaseName()
    {
        echo $this->dbname;
    }
}
