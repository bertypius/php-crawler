<?php

namespace App\Model;

use App\Eloquent\Model;

class Product extends Model
{
    protected $dbtable = 'products';
}
