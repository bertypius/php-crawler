<?php
// ini_set("display_errors", 1);
header("Access-Control-Allow-Origin: *"); //allow all origins eg localhost, anydomain
header("Content-Type: application/json; charset=UTF-8"); //receive content in json format
header("Access-Control-Allow-Methods:PATCH"); //allow POST requests

require "../../autoloader.php";

use App\Model\Product;

if ($_SERVER["REQUEST_METHOD"] === "PATCH") {
    $data = json_decode(file_get_contents("php://input"), true);
    $product = new Product();
    if (!empty($data["id"]) && !empty($data["name"]) && !empty($data["image"]) && !empty($data["initialPrice"]) && !empty($data["currentPrice"])) {
        $saved = $product->updateData($data);
        if ($saved) {
            http_response_code(200); // ok response code
            echo json_encode(array(
                "status" => $product->status,
                "message" => $product->apiMessage
            ));
        } else {
            http_response_code(500); //internal server error
            echo json_encode(array(
                "status" => $product->status,
                "message" => $product->apiMessage
            ));
        }
    } else {
        http_response_code(404);    // Service unavailable
        echo json_encode(array(
            "status" => "error",
            "message" => "All fields are required to add a new product"
        ));
    }
} else {
    http_response_code(405);
    echo json_encode(array(
        "status" => "error",
        "message" => "Request method not allowed"
    ));
}
