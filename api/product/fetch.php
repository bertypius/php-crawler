<?php
ini_set('display_errors', 1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods:GET');


require '../../autoloader.php';

use App\Model\Product;

if (($_SERVER['REQUEST_METHOD'] === 'GET')) {
    $product = new Product();
    if (isset($_GET["id"]) && !empty($_GET["id"])) {
        $id = $_GET["id"];
        $data = $product->getData(id: $id);
    } else {
        $data = $product->getData();
    }
    if ($data) {
        http_response_code(200);
        echo json_encode(
            array(
                "status" => $product->status,
                "data" => $data
            )
        );
    } else {
        http_response_code(404);
        echo json_encode(
            array(
                "status" => $product->status,
                "data" => $data
            )
        );
    }
} else {
    http_response_code(405);
    echo json_encode(
        array(
            "status" => "error",
            "message" => "Request method not allowed"
        )
    );
}