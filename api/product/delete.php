<?php
ini_set('display_errors', 1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods:GET,DELETE');


require '../../autoloader.php';

use App\Model\Product;

if (($_SERVER['REQUEST_METHOD'] === 'GET') || ($_SERVER['REQUEST_METHOD'] === 'DELETE')) {
    $product = new Product();
    if (isset($_GET["id"]) && !empty($_GET["id"])) {
        $id = $_GET["id"];
        if (!is_numeric($id)) {
            http_response_code(200);
            echo json_encode(
                array(
                    "status" => "failed",
                    "message" => "Bad request,the id is not valid",
                )
            );
            die();
        } else {
            $res = $product->deleteData($id);
            if ($res) {
                http_response_code(200);
                echo json_encode(
                    array(
                        "status" => $product->status,
                        "message" => $product->apiMessage
                    )
                );
            } else {
                http_response_code(200);
                echo json_encode(
                    array(
                        "status" => $product->status,
                        "message" => $product->apiMessage
                    )
                );
            }
        }
    } else {
        http_response_code(200);
        echo json_encode(
            array(
                "status" => "error",
                "message" => "provide query parameters eg id"
            )
        );
    }
} else {
    http_response_code(405);
    echo json_encode(
        array(
            "status" => "error",
            "message" => "Request method not allowed"
        )
    );
}