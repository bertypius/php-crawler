<?php

namespace Crawler\Repository;

ini_set('track_errors', '1');

use Regex\Interfaces\RegexPattern;

abstract class Content
{
    protected $curl_url = "";
    abstract function formatContent(RegexPattern $pattern);
    public function getContent($url)
    {
        $this->curl_url = $url;
        $curl = \curl_init();
        \curl_setopt($curl, CURLOPT_URL, $this->curl_url);
        \curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        \curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = \curl_exec($curl);
        curl_close($curl);

        return $result;
    }
    public function downloadImage(string $image, string $storage, int $counter)
    {
        $storage = $this->storage($storage);
        $search = ['\.jpg', '\.png', '\.Webp', '\.gif', '\.jpeg'];
        $implode = "(" . implode("|", $search) . ")";
        $pattern = "!$implode!";
        preg_match($pattern, $image, $match);
        $savedImage = $counter . $match[1];

        // $content = file_get_contents($image);
        // file_put_contents("App/Http/Kernel/$savedImage", $content);
        try {
            @copy($image, "$storage/$savedImage");
            throw new \Exception("copy(): SSL operation failed with code 1. OpenSSL Error messages: error:0A000126:SSL routines::unexpected eof while reading in /var/www/html/php-crawler/Crawler/Repository/Content.php on line 37");
        } catch (\Throwable $e) {
            $message = $e->getMessage() . "\n";
            error_log($message, 3, "errors.log");
        }
        return $savedImage;
    }
    public function storage($storage)
    {
        if (!is_dir($storage)) {
            echo "Your storage location is invalid, please provide a valid directory where your downloads will be stored";
            exit;
        } elseif (!is_writable($storage)) {
            echo "Permission denied!, please provide a writable directory";
            exit;
        } else {
            return $storage;
        }
    }
}