<?php

namespace Crawler\Http;

use Crawler\Repository\Content;
use Regex\Interfaces\RegexPattern;

class Request extends Content
{
    protected $data;
    protected $pattern;
    protected $products = array();
    public function makeRequest($url, $pattern, bool $display = false)
    {
        $this->data = $this->getContent($url);
        if ($display == true) {
            echo $this->data;
        }
        return $this->formatContent($pattern);
    }
    public function formatContent(RegexPattern $pattern)
    {
        $this->pattern = $pattern->getPattern();
        preg_match_all($this->pattern, $this->data, $matches);
        $this->products = array_map(null, $matches[1], $matches[2], $matches[3], $matches[4]);
        // print_r($this->products);
        return $this->products;
    }
}