<?php

namespace Crawler\Http;

use Crawler\Repository\Content;
use Regex\Interfaces\RegexPattern;

class Response extends Content
{
    protected $data;
    protected $pattern;
    public function makeRequest($url, bool $display = false)
    {
        $this->data = $this->getContent($url);
        if ($display == true) {
            echo $this->data;
        }
        return $this->data;
    }
    public function formatContent(RegexPattern $pattern)
    {
        $this->pattern = $pattern->getPattern();
        if (preg_match_all($this->pattern, $this->data, $matches)) {
            return $matches;
        } else {
            echo "Error: Something went wrong with the pattern. Please try again";
        }
    }
}