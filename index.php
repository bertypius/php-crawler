<?php
ini_set("display_errors", 1);
require "autoloader.php";

use App\Http\Controller\ProductController;

$product = new ProductController();
$product->crawlProduct();
$data = $product->allProducts();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Crawler</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container">

        <?php
        if (isset($data)) {
            foreach ($data as $val) {
                echo "<div class=\"card\">
                    <div class='pImage'>
                         <img src='$val->image'/>
                    </div> 
                    <div class='product_details'>
                        <p class='product_desc'>$val->name</p>
                        <p class='price_ad'>$val->currentPrice</p>
                        <p class='price_bd'>$val->initialPrice</p>
                    </div>
            </div>";
            }
        }
        ?>

    </div>
</body>

</html>