<?php

namespace Regex\Pattern;

use Regex\Interfaces\RegexPattern;

class Vishakid implements RegexPattern
{
    protected $pattern = "!https://vishakid.com/uploads/.+?\.[a-zA-Z]{3,4}!";
    // protected $pattern = "/(https\:\/\/vishakid\.com\/uploads\/).+\.[a-zA-Z]{3,4}/";
    public function createPattern($pattern = null)
    {
        if ($pattern !== null) {
            $this->pattern = $pattern;
        }
    }
    public function getPattern()
    {
        return $this->pattern;
    }
}