<?php

namespace Regex\Pattern;

use Regex\Interfaces\RegexPattern;

class Jumia implements RegexPattern
{
    protected $product = array();
    // protected $pNamePattern = '!<div class="name">.*?</div>!';
    // protected $pPricePattern = '!<div class="prc" data-oprc=(.*?)>(.*?)<\/div>!';
    // protected $pImgPattern = "/(https\:\/\/ke\.jumia\.is\/unsafe\/fit\-in\/300x300\/filters\:fill\(white\)\/product\/\d{2}\/\d{2,}?\/\d{1}\.jpg\?\d{4}/";
    protected $megaPattern = '/<a class="core" href=.*? data-id=.*? data-name="(.*?)" data-price=.*?\s.*?(https\:\/\/ke\.jumia\.is\/unsafe\/fit\-in\/300x300\/filters\:fill\(white\)\/product\/\d{2}\/\d{2,}?\/\d{1}\.jpg\?\d{4}).*?<div class="prc" data-oprc="(.*?)">(.*?)<\/div><div class="bdg _dsct">(.*?)<\/div><\/a>/';

    // protected $pattern = "!https://ke.jumia.is/unsafe/fit-in/300x300/filters:fill(white)/product/\d{2}/\d{2,}?/\d{1}.jpg?\d{4}!";
    public function createPattern($pattern = null)
    {
        if ($pattern !== null) {
            $this->megaPattern = $pattern;
        }
    }
    public function getPattern()
    {
        return $this->megaPattern;
    }
}
