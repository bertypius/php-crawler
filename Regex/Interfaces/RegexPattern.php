<?php

namespace Regex\Interfaces;

interface RegexPattern
{
    public function createPattern();
    public function getPattern();
}